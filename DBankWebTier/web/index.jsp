<%-- 
    Document   : index
    Created on : 17-Jun-2016, 16:53:18
    Author     : Selvyn
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="globalHelper" class="deutschebank.thebeans.ApplicationScopeHelper" scope="application"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="js/jquery-3.0.0.js"></script>
        <script type="text/javascript" src="js/userdetails_validator.js"></script>
        <script type = "text/javascript" > disableBackBtn(); </script>
        <title>Deutsche Bank Case Study</title>
        <link href="login.css" rel="stylesheet" type="text/css">
    </head>
    
    <body>
        <%
            String  dbStatus = "DB NOT CONNECTED";

            globalHelper.setInfo("Set any value here for application level access");
            boolean connectionStatus = globalHelper.bootstrapDBConnection();
            
            if( connectionStatus )
            {
                dbStatus = "";
            }
        %>
        <h2><%= dbStatus %></h2>
        <%
            if( connectionStatus )
            {
        %>
        <div class = "container">
            
            <div class="wrapper">
                
                <form action = "bootstrap.jsp" method = "GET" name="Login_Form" class="form-signin">
                    <h3>Sign In</h3>
                    <div class="usidpwd">
                        <input placeholder="User ID"type="text" class="form-control" id="f_userid" name="id">
                        <input placeholder="Password" type="password" class="form-control" id="f_pwd" name="password">
                    </div>
                    <div class="form-group" id="b">
                        <button class="btn" type="button" onclick="validateUserId()">Verify</button>
                        <input  class="btn" type="hidden" id="mySubmit"/>
                    </div>
                    <div id="userIdMessage"> </div>
                </form>
                <p>
                    
                </p>
            </div>
        </div>        
        <%
            }
        %>
    </body>
</html>
